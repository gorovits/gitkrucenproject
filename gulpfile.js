var gulp = require('gulp');
var image = require('gulp-image');
var jade = require('gulp-jade');
var sass = require('gulp-sass');
var connect = require('gulp-connect');
var plumber = require('gulp-plumber');
var notify = require('gulp-notify');

var outputDir = "build";
var inputDir = "src";

gulp.task("image", function(){
	gulp.src(
			[inputDir + '/files/img/*.png',
					inputDir + '/files/img/*.jpg',
					inputDir + '/files/img/*.jpeg',
					inputDir + '/files/img/*.gif',
					inputDir + '/files/img/*.tiff',
					inputDir + '/files/img/*.webp',
					inputDir + '/files/img/*.svg'])
		.pipe(plumber({errorHandler: notify.onError("Error: <%= error.message %>")}))
		.pipe(image())
		.pipe(gulp.dest(inputDir + '/img/'))
		.pipe(connect.reload())
})

gulp.task("html", function(){
	gulp.src(inputDir + '/templates/*.jade')
		.pipe(plumber({errorHandler: notify.onError("Error: <%= error.message %>")}))
		.pipe(jade())
		.pipe(gulp.dest(outputDir + '/html'))
		.pipe(connect.reload())
})

gulp.task("css", function(){
	gulp.src(inputDir + '/scss/*.scss')
		.pipe(plumber({errorHandler: notify.onError("Error: <%= error.message %>")}))
		.pipe(sass())
		.pipe(gulp.dest(outputDir + '/css'))
		.pipe(connect.reload())
})

gulp.task("watch", function(){
	gulp.watch([ inputDir + '/files/img/*.jpg', 'src/img/*.png' ])
	gulp.watch(inputDir + '/templates/*.jade', ['html'])
	gulp.watch(inputDir + '/scss/*.scss', ['css'])
})

gulp.task('connect', function(){
	connect.server({
		port: 9000,
		livereload: true,
		root: 'build'
	})
})

gulp.task("default", ['connect','html','css','image','watch'])
